# Printing card
This is a utility lib to allow me to make consistent card image that are meant to be printed. It use the bridge size card from the website [printer studio](https://www.printerstudio.com/personalized/bridge-size-custom-cards-blank-cards.html).

## Use
See cards.py main part that is a test for an example of use.


# Font Credits
 - [Graduate](https://fonts.google.com/specimen/Graduate) for cards numbers.
 - [PT Sans Narrow](https://fonts.google.com/specimen/PT+Sans+Narrow) for credits.
 - [Anquietas](https://www.wfonts.com/font/anquietas) for deck id.
