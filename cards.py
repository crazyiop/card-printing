from PIL import Image, ImageFont, ImageDraw
from pathlib import Path
import qrcode
from tqdm import tqdm
from math import ceil
from fpdf import FPDF as PDF
from tempfile import mkdtemp

grey = (100, 100, 100)
black = (0, 0, 0)
PATH = Path(__file__).resolve(strict=True).parent


def img_merge(bg_img, fg_img, coord=(0, 0)):
    # inspire by https://stackoverflow.com/a/53663233
    # merge top left corner of fg_img at coord
    fg_img_trans = Image.new("RGBA", fg_img.size)
    fg_img_trans = Image.blend(fg_img_trans, fg_img, 1)
    bg_img.paste(fg_img_trans, coord, fg_img_trans)


def mk_header(big, little, size=None):
    if size is None:
        size = Card.SAFE[0]
    header = Image.new("RGBA", (size, 100))
    draw = ImageDraw.Draw(header)

    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 100)
    if big:
        draw.text((0, 0), str(big), black, font=font)
        l, t, r, b = draw.textbbox((0, 0), str(big), font=font)
    else:
        l, t, r, b  = draw.textbbox((0, 0), "00", font=font)
    bigx = b - t
    bigy = r - l

    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 50)
    l, t, r, b = draw.textbbox((0, 0), " " + str(little), font=font)
    littlex = b - t
    littley = r - l
    draw.text((bigx, bigy - littley), " " + str(little), grey, font=font)
    return header


def mk_footer(txt, size=None, color=None, font=None):
    font_size = 40
    if size is None:
        size = Card.SAFE[0]
    if color is None:
        color = grey
    if font is None:
        font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), font_size)
    footer = Image.new("RGBA", (size, font_size))
    draw = ImageDraw.Draw(footer)
    tx, ty = draw.textsize(str(txt), font=font)
    draw.text(((size - tx) // 2, (font_size - ty) // 2), str(txt), color, font=font)
    return footer


class Card:
    # Default = Bridge card size from printerstudio
    BLEED = (747, 1122)
    CUT = (675, 1050)
    SAFE = (615, 990)

    window_box = (189, 336, 747 - 189, 1122 - 177)

    safe_margin = (BLEED[0] - SAFE[0]) // 2, (BLEED[1] - SAFE[1]) // 2
    cut_margin = int((BLEED[0] - CUT[0]) // 2), int((BLEED[1] - CUT[1]) // 2)

    def __init__(self, parts=None, deck_id=None, debug=False, mode="normal"):
        self._img = Image.new("RGBA", self.BLEED)
        self.draw = ImageDraw.Draw(self._img)
        self.deck_id = deck_id
        self.debug = debug or '' # comma separated: part, safe, cut, window
        self.mode = mode

        if parts:
            self.parts = parts
        else:
            self.parts = []

    def add(self, img):
        self.parts.append(img)

    def _gen(self):
        used_y = sum(p.size[1] for p in self.parts)
        nb_split = len(self.parts) + 1
        y_left = self.BLEED[1] - used_y
        if "top" not in self.mode:
            y_left -= Card.safe_margin[1]
            y = Card.safe_margin[1]
        else:
            y = 0
            nb_split -= 1
        if "bottom" not in self.mode:
            y_left -= Card.safe_margin[1]
        else:
            nb_split -= 1

        y_margin = y_left // nb_split

        for p, part in enumerate(self.parts):
            ix, iy = part.size
            x = Card.safe_margin[0] + (self.SAFE[0] - ix) // 2
            if "bottom" in self.mode and part == self.parts[-1]:
                y = self.BLEED[1] - iy
            else:
                y += y_margin

            img_merge(self._img, part, coord=(x, y))
            if 'part' in self.debug:
                self.draw.rectangle([x, y, x + ix, y + iy], outline="green")

            y += iy

        # Add a deck id
        if self.deck_id:
            print(self.deck_id)
            font = ImageFont.truetype(str(PATH / "anquietas.ttf"), 40)
            tx, ty = self.draw.textsize(self.deck_id, font=font)
            self.draw.text(
                (
                    self.BLEED[0] - Card.safe_margin[0] - tx * 1.5,
                    self.BLEED[1] - Card.safe_margin[1] - ty * 1.5,
                ),
                self.deck_id,
                grey,
                font=font,
            )

        font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 20)
        if 'safe' in self.debug:
            self.draw.rectangle(
                [
                    *Card.safe_margin,
                    Card.safe_margin[0] + self.SAFE[0],
                    Card.safe_margin[1] + self.SAFE[1],
                ],
                outline="black",
            )
            _, _, tx, ty = self.draw.textbbox((0,0), 'safe', font=font)
            self.draw.text(
                (Card.safe_margin[0], Card.safe_margin[1]-ty),
                "safe",
                black,
                font=font,
            )
        if 'cut' in self.debug:
            _, _, tx, ty = self.draw.textbbox((0,0), 'cut', font=font)
            self.draw.text(
                (Card.cut_margin[0], Card.cut_margin[1]-ty),
                "cut",
                black,
                font=font,
            )
            self.draw.rectangle(
                [
                    *Card.cut_margin,
                    Card.cut_margin[0] + self.CUT[0],
                    Card.cut_margin[1] + self.CUT[1],
                ],
                outline="black",
            )
        if 'mark' in self.debug:
            x1, y1 = Card.cut_margin
            x2 = Card.cut_margin[0] + self.CUT[0]
            y2 = Card.cut_margin[1] + self.CUT[1]

            h = 15
            l = 30

            self.draw.line((x1, y1-h, x1, y1-h-l), fill = "black")
            self.draw.line((x2, y1-h, x2, y1-h-l), fill = "black")

            self.draw.line((x1, y2+h, x1, y2+h+l), fill = "black")
            self.draw.line((x2, y2+h, x2, y2+h+l), fill = "black")

            self.draw.line((x1-h, y1, x1-h-l, y1), fill = "black")
            self.draw.line((x1-h, y2, x1-h-l, y2), fill = "black")

            self.draw.line((x2+h, y1, x2+h+l, y1), fill = "black")
            self.draw.line((x2+h, y2, x2+h+l, y2), fill = "black")


        if 'window' in self.debug:
            self.draw.rectangle(self.window_box, outline="red")

    def show(self):
        self._gen()
        self._img.show()

    def save(self, *args, **kwargs):
        self._gen()
        self._img.save(*args, **kwargs)

    @property
    def img(self):
        self._gen()
        return self._img


def _find_font_size(size):
    # find the max font size to write the number tick given the box size
    x, y = size
    draw = ImageDraw.Draw(Image.new("RGBA", size))
    font_size = 100
    ty = 999_999
    while ty * 1.5 > y or ty * 3.5 > x:
        font_size -= 1
        font = ImageFont.truetype(
            str(PATH / "Graduate-Regular.ttf"),
            font_size,
        )
        tx, ty = draw.textsize("M", font=font)
    width = int(ty * 1.5) + 2 * tx
    ori = (x - width) // 2, (y - ty) // 2
    return font_size, ori


def _mk_number_tick(n, size):
    number = Image.new("RGBA", size)
    draw = ImageDraw.Draw(number)

    font_size, ori = _find_font_size(size)
    ox, oy = ori
    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), font_size)
    tx, ty = draw.textsize("M", font=font)

    # Center the thing
    draw.rectangle([ox, oy, ox + ty, oy + ty], None, black)
    draw.text(
        [ox + int(ty * 1.5), oy, ox + int(ty * 2.5), oy + ty], str(n), black, font=font
    )
    return number


def mk_tick_card(id_list, bg=None, col=3):
    r = Image.new("RGBA", Card.BLEED)
    v = Image.new("RGBA", Card.BLEED)

    # we need to fit n+1 cell (for name)
    id_list = list(id_list)
    n = len(id_list)
    nb_rows = ceil((n + 1) / col)
    rows_recto = nb_rows // 2
    rows_verso = nb_rows - rows_recto

    st = (Card.SAFE[0] // 3, Card.SAFE[1] // rows_recto)

    draw = ImageDraw.Draw(r)
    font_size, ori = _find_font_size(st)
    ox, oy = ori
    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), font_size)

    # recto
    for y in range(rows_recto):
        for x in range(col):
            i = y + x * rows_recto
            if i == 0:
                draw.text(
                    [Card.safe_margin[0] + 10, Card.safe_margin[1] + oy],
                    ">",
                    black,
                    font=font,
                )
                pass
            else:
                number = _mk_number_tick(id_list[i - 1], st)
                img_merge(
                    r,
                    number,
                    (Card.safe_margin[0] + x * st[0], Card.safe_margin[1] + y * st[1]),
                )
    start = i + 1

    # recto
    for y in range(rows_verso):
        for x in range(col):
            i = y + x * rows_verso + start
            if i <= n:
                number = _mk_number_tick(id_list[i - 1], st)
                img_merge(
                    v,
                    number,
                    (Card.safe_margin[0] + x * st[0], Card.safe_margin[1] + y * st[1]),
                )

    if bg is not None:
        cbg = bg.copy()
        cbg.putalpha(256 // 4)
        r = Image.alpha_composite(cbg, r)
        v = Image.alpha_composite(cbg, v)
    return r, v


def mk_md(markdown, qrcode_url=None):
    # TODO see if using https://github.com/vgalin/html2image would not be better...

    # content only first
    card = Image.new("RGBA", Card.SAFE)
    draw = ImageDraw.Draw(card)

    base_font_size = 25
    currentx, currenty = 15, 15

    for line in markdown.split("\n"):
        text = line.strip()
        if not text:
            currenty += base_font_size // 2
        else:
            font_size = base_font_size + [0, 6, 8][text.count("#")]
            indent = base_font_size * [2, 0, 0][text.count("#")]
            text = text.strip("#")
            font = ImageFont.truetype(
                str(PATH / "PT_Sans-Narrow-Web-Regular.ttf"),
                font_size,
            )
            tx, ty = draw.textsize(text, font=font)
            draw.text((currentx + indent, currenty), text, "black", font=font)
            currenty += ty + ty // 4  # 125% interline

    if qrcode_url:
        qr = qrcode.QRCode(box_size=7)
        qr.add_data(qrcode_url)
        qr.make(fit=True)
        qr_img = qr.make_image().convert("RGBA")

        qrx = (Card.SAFE[0] - qr_img.size[0]) // 2
        qry = currenty + (Card.SAFE[1] - currenty - qr_img.size[1]) // 2
        img_merge(card, qr_img, (qrx, qry))

    full_card = Image.new("RGBA", Card.BLEED)
    img_merge(full_card, card, Card.safe_margin)
    return full_card


class Deck:
    X, Y = 2.49, 3.74

    def __init__(self, name, imgs=None):
        self.name = name
        self.dir = mkdtemp()
        self.dir = Path(f'/tmp/cards/{name}')
        self.dir.mkdir(parents=True, exist_ok=True)
        if not imgs:
            self.imgs = []
        else:
            self.imgs = imgs

    def add(self, img):
        self.imgs.append(img)

    def pdf(self, title="custom game deck", author="crazyiop@gitlab"):
        pdf = PDF(unit="in", format=(Deck.X, Deck.Y))
        pdf.set_title(title)
        pdf.set_author(author)


        print(f"generating {self.name}.pdf:")
        t = tqdm(total=len(self.imgs))
        for i, img in enumerate(self.imgs, start=1):
            t.update(1)
            cardfile = f"{self.dir}/tmpcard{i:>02}.png"
            img.save(cardfile)
            pdf.add_page()
            pdf.image(cardfile, w=Deck.X, x=0, y=0)
        t.close()
        pdf.output(f"{self.name}.pdf")

    """
    def a4pdf(deck, deck_name):
        X, Y = 2.49, 3.74
        a4x = 8.27
        a4y = 11.69
        pdf = PDF(unit="in", format=(a4x, a4y))

        pdf.set_title("Rush hour extension deck")
        pdf.set_author("Jonathan Nifenecker")

        pdf.add_page()
        for i, card in enumerate(deck[:9]):
            y, x = divmod(i, 3)
            print(card["filename"])
            pdf.image(str(card["filename"]), w=2.49, x=x*a4x/3, y=y*a4y/3)
        pdf.output(f"{deck_name}-a4.pdf")
    """

    def png(self):
        path = Path('pngs') / self.name
        recto = path/'recto'
        recto.mkdir(parents=True, exist_ok=True)
        verso = path/'verso'
        verso.mkdir(parents=True, exist_ok=True)
        t = tqdm(total=len(self.imgs))
        for i, img in enumerate(self.imgs, start=1):
            t.update(1)
            if i%2:
                fp = recto/ f"{i:>02}.png"
            else:
                fp = verso/ f"{i:>02}.png"
            img.save(fp)
        t.close()

if __name__ == "__main__":

    def mk_content(n):
        content = Image.new("RGBA", (n, n))
        draw = ImageDraw.Draw(content)
        draw.rectangle([0, 0, n, n], fill="grey")
        return content

    c = Card(deck_id="abc", debug=True)
    c.add(mk_header(big="big", little="little"))
    c.add(mk_content(600))
    c.add(mk_footer("#reference"))
    c.show()
    d = Deck([c], name="test")
    d.pdf()
